var mongoClient = require("mongodb").MongoClient;
mongoClient.connect("mongodb://localhost/sprint", { useNewUrlParser: true })
            .then(conn => global.conn = conn.db("sprint"))
            .catch(err => console.log(err))

function findAll(callback){  
    global.conn.collection("persons").find({}).toArray(callback);
}

function insert(person, callback){
    global.conn.collection("persons").insert(person, callback);
}

var ObjectId = require("mongodb").ObjectId;
function findOne(id, callback){  
    global.conn.collection("persons").find(new ObjectId(id)).toArray(callback);
}

function updateOne(id, person, callback){
    global.conn.collection("persons").update({
        _id:new ObjectId(id)},
        person,
        callback
        );

}

function deleteOne(id, callback){
    global.conn.collection("persons").remove({_id: new ObjectId(id)}, callback);
}

module.exports = { findAll, insert, findOne, updateOne, deleteOne }
